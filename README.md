# Follow Me

Life is precious and so is time. So, make the most of it by logging every aspect of your life using our “FollowMe” application. Structured to remind you about everyday movements, “FollowMe” divides tracking broadly into work, home, on-road, errands and recreational categories. It runs as a background application and needs access to the location services. You can get aggregated information for time spent on each of these categories so that you can plan and optimize things accordingly. 

---

### API / Libraries Used

* Map Kit
* Google SDK (Signin, Autocomplete)
* Back4app
* CocoaPods

### Configurations

* Create OAuth client ID for google signin - [Link](https://developers.google.com/identity/sign-in/ios/start?ver=swift)
* Configure back4app - [Link](https://www.back4app.com/docs/parse-create-new-app)
* Install CocoaPods -  [Link](https://guides.cocoapods.org/making/index.html)
* Clone the git repository
* Open command prompt
* Run "pod install"
* Open workspace and play with it

### Whom to contact

* Abhijeet Prakash Agrawal (s530670)
* Kishan Kalburgi Srinivas (s530468)
* Krishna Veni Karri (s530471)
* PhaniVardhan Gurram (s530743)