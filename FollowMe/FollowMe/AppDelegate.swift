//
//  AppDelegate.swift
//  FollowMe
//
//  Created by student on 2/15/18.
//  Copyright © 2018 temM5. All rights reserved.
//

import UIKit
import GoogleSignIn

import Parse

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, GIDSignInDelegate {
    

    var window: UIWindow?
    static var model: DataModel?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        //Database Config
        let configuration = ParseClientConfiguration {
            $0.applicationId = "Yh2Lx4LbkYuiDIA3CAvfqNFS8AZENSwmfdlH6hmf"
            $0.clientKey = "lsesu3gWhg9l9D55cyxiO2gVMceg6bVVpHRdggKu"
            $0.server = "https://parseapi.back4app.com"
        }
        Parse.initialize(with: configuration)
        saveInstallationObject()
        // Override point for customization after application launch.
        GIDSignIn.sharedInstance().clientID = "823445969114-vf7dnme8fuevjgqnnlubf98ofsdddv85.apps.googleusercontent.com"
        GIDSignIn.sharedInstance().delegate = self
        AppDelegate.model = DataModel()
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        return GIDSignIn.sharedInstance().handle(url, sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String, annotation: options[UIApplicationOpenURLOptionsKey.annotation])
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let error = error {
            print("\(error.localizedDescription)")
        } else {
            let logUser = User();
            // Perform any operations on signed in user here.
            //let userId = user.userID                  // For client-side use only!
            //let idToken = user.authentication.idToken // Safe to send to the server
            logUser.firstName = user.profile.givenName
            logUser.lastName = user.profile.familyName
            logUser.email = user.profile.email
            saveUserInfo(logUser)
            print("======================")
            print(logUser)
             print("======================")
          
            
        }
    }

    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        print("User got disconnected")
    }
    
    //Test the connection
    func saveInstallationObject(){
        if let installation = PFInstallation.current(){
            installation.saveInBackground {
                (success: Bool, error: Error?) in
                if (success) {
                    print("---------You have successfully connected your app to Back4App!")
                } else {
                    if let myError = error{
                        print(myError.localizedDescription)
                    }else{
                        print("Uknown error")
                    }
                }
            }
        }
    }
    
    func saveUserInfo(_ user : User){
        let query = PFQuery(className:"LoggedUser")
        query.whereKey("email", equalTo:user.email)
        query.findObjectsInBackground {   // what happened to the ( ) ?
            (objects: [PFObject]?, error: Error?) -> Void in
            if error == nil {
                // The find succeeded.
                print("Count\(objects?.count)")
                if objects!.count != 0 {
                    print("Old User")
                    user.objectId = objects![0].objectId!
                    AppDelegate.model?.currUser  = user;
                    print(user)
                    self.redirectToHomePage()
                }else{
                    print("New User")
                    user.saveInBackground(block: { (success, error) -> Void in
                        if success {
                            print("User saved.")
                             AppDelegate.model?.currUser  = user;
                            print("===\(user.objectId)")
                            self.redirectToHomePage()
                        } else {
                            print("error!!!")
                        }
                    })
                }
            } else {
                // Log details of the failure
                print("Oops \(error!)")
            } }
    }
    
    func redirectToHomePage(){
        let mainStoryboardIpad : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let initialViewControlleripad : UITabBarController = mainStoryboardIpad.instantiateViewController(withIdentifier: "tabView") as! UITabBarController
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window?.rootViewController = initialViewControlleripad
        self.window?.makeKeyAndVisible()
        
    }

}

