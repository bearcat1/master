//
//  LoginViewController.swift
//  FollowMe
//
//  Created by Kalburgi Srinivas,Kishan on 3/7/18.
//  Copyright © 2018 temM5. All rights reserved.
//

import UIKit
import GoogleSignIn
import FacebookCore
import FacebookLogin

class LoginViewController: UIViewController, GIDSignInUIDelegate {
    
    @IBOutlet weak var signInButton: GIDSignInButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Google sign in button add
        let googleSignInButton = GIDSignInButton()
        googleSignInButton.frame = CGRect(x: 60, y: 360, width: view.frame.width - 115, height: 50)
        view.addSubview(googleSignInButton)
       
        GIDSignIn.sharedInstance().uiDelegate = self
        //Fb sign in button
        let fbLoginButton = LoginButton(readPermissions: [ .publicProfile ])
        fbLoginButton.frame = CGRect(x: 60, y: 300, width: view.frame.width - 115, height: 40)
        view.addSubview(fbLoginButton)
        
         // Add a custom login button to your app
//        let myLoginButton = UIButton(type: .custom)
//        myLoginButton.backgroundColor = UIColor.darkGray
//        myLoginButton.frame = CGRect(x: 60, y: 300, width: view.frame.width - 115, height: 40)
//        myLoginButton.center = view.center;
//        myLoginButton.setTitle("Login In", for: .normal)
//        myLoginButton.addTarget(self, action: #selector(self.loginButtonClicked), for:.touchUpInside)
        //myLoginButton.addTarget(self, action: <#Selector#>, action: @selector(self.loginButtonClicked) forControlEvents: .TouchUpInside)
        //view.addSubview(myLoginButton)
        
    }

    // Once the button is clicked, show the login dialog
    @objc func loginButtonClicked() {
        let loginManager = LoginManager()
        loginManager.logIn(readPermissions: [.publicProfile], viewController: self, completion: { loginResult in
            switch loginResult {
            default:
                print("Logged in!")
            }
        })
    }
        
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let error = error {
            print("\(error.localizedDescription)")
        } else {
            // Perform any operations on signed in user here.
            let userId = user.userID                  // For client-side use only!
            let idToken = user.authentication.idToken // Safe to send to the server
            //            let fullName = user.profile.name
            //            let givenName = user.profile.givenName
            //            let familyName = user.profile.familyName
            //            let email = user.profile.email
            print(userId)
            print(idToken)
            
            let homeVC:HomeViewController = HomeViewController()
            self.present(homeVC, animated: true, completion: nil)
        }
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        print("User got disconnected")
    }
    

}
