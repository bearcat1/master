//
//  Model.swift
//  FollowMe
//
//  Created by Student on 3/9/18.
//  Copyright © 2018 temM5. All rights reserved.
//

import Foundation
import Parse
import MapKit

class Favourite:PFObject, PFSubclassing {
    @NSManaged var userId : String
    @NSManaged var latitude : Double
    @NSManaged var longitude : Double
    @NSManaged var street : String
    @NSManaged var city : String
    @NSManaged var state : String
    @NSManaged var country : String
    @NSManaged var title : String
    @NSManaged var address : String
    static func parseClassName() -> String {
        return "Favourite"
    }
}

class DataModel{
    var currUser : User
    
    init() {
        currUser = User()
    }
}

class User:PFObject, PFSubclassing {
    @NSManaged var email : String
    @NSManaged var firstName : String
    @NSManaged var lastName : String
    static func parseClassName() -> String {
        return "LoggedUser"
    }
}


class PlacesVisited:PFObject, PFSubclassing {
    @NSManaged var placeId : String
    @NSManaged var userId : String
    @NSManaged var date : String
    @NSManaged var timeSpent : Double
    @NSManaged var placeTitle : String
    static func parseClassName() -> String {
        return "PlacesVisited"
    }
}

struct LocationData {
    var lName : String
    var lLatitude : Double
    var lLongitude : Double
    var lAddress : String
    var isFavourite : Bool
    var enterDateTime : String
    var leaveDateTime : String
}

class UserPlaces {
    var colPlaces : [LocationData]
    var userInfo : User
    init() {
        colPlaces = []
        userInfo = User()
    }
    
    func addPlace(newPlace : LocationData){
        colPlaces.append(newPlace)
    }
    
    func fetchAllFavouritePlaces() -> [LocationData]{
        var temp : [LocationData] = []
        for i in colPlaces{
            if i.isFavourite == true{
                temp.append(i)
            }
        }
        return temp;
    }
    
    //In-Progress
    func fetchDateWiseData(){
        
    }
    
    //In-Progress
    func fetchStatisticsDetails(){
        
    }
    
    //In-Progress
    func changeUserSetttings(){
        
    }
}

//Model for Timeline
class AnnotationPin: NSObject, MKAnnotation {
    var title: String?
    var subtitle: String?
    var coordinate: CLLocationCoordinate2D
    
    init(title: String, Subtitle: String, coordinate: CLLocationCoordinate2D) {
        self.title = title
        self.subtitle = Subtitle
        self.coordinate = coordinate
    }
}

