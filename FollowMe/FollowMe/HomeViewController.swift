//
//  HomeViewController.swift
//  FollowMe
//
//  Created by Student on 3/9/18.
//  Copyright © 2018 temM5. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import Parse

class HomeViewController: UIViewController, CLLocationManagerDelegate  {
    
    @IBOutlet weak var mapView: MKMapView!
    let manager = CLLocationManager()
    var timer = Date()
    override func viewDidLoad() {
        super.viewDidLoad()
       
        // Do any additional setup after loading the view.
        manager.delegate = self
        manager.requestAlwaysAuthorization()
        manager.startUpdatingLocation()
        manager.distanceFilter = 100;
        attachLocationFencing()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        timer = Date()
        print("Enter  \(region.identifier)")
    }
    
    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
        let diffTime = Date().timeIntervalSince(timer)
        let user = AppDelegate.model?.currUser
        let dateStr:String = String(describing: timer)
        var dateA  = dateStr.split(separator : " ")
        let place = PlacesVisited()
        place["userId"] = user!.objectId
        
        var split = region.identifier.split(separator: "-")
        print(split)
        place.placeId = String(split[1])
        place.placeTitle = String(split[0])
        place.timeSpent = diffTime
        
        place.date = String(dateA[0])
        
        timer = Date()
        place.saveInBackground(block: { (success, error) -> Void in
            if success {
                print("Place visit log added.")
            } else {
                print("error!!!")
            }
        })
        print("Exit  \(region.identifier) === \(diffTime)")
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations [0]
        let span: MKCoordinateSpan = MKCoordinateSpanMake(0.01, 0.01)
        let myLocation: CLLocationCoordinate2D = CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude)
        //print(myLocation)
        let region: MKCoordinateRegion = MKCoordinateRegionMake(myLocation, span)
        mapView.setRegion(region, animated: true)
        
        self.mapView.showsUserLocation = true
        
        CLGeocoder().reverseGeocodeLocation(location) { (placemark, error) in
            if error != nil
            {
                print ("THERE WAS AN ERROR")
            }
            else
            {
                if let place = placemark?[0]
                {
                    if place.subThoroughfare != nil
                    {
                        print("\(place.subThoroughfare!) \n \(place.thoroughfare!) \n \(place.country!)")
                    }
                }
            }
        }
    }
    
    func attachLocationFencing(){
        var favs : [Favourite] = []
        let query = PFQuery(className:"Favourite")     // Fetches all the Movie objects
        let user = AppDelegate.model?.currUser
        query.whereKey("userId", equalTo: user!.objectId ?? "" )
        query.findObjectsInBackground {   // what happened to the ( ) ?
            (objects: [PFObject]?, error: Error?) -> Void in
            if error == nil {
                // The find succeeded.
                favs = objects as! [Favourite]
               
                for fav in favs{
                    let str = fav.title + "-" + String(describing: fav.objectId!)
                    let geoFencing:CLCircularRegion = CLCircularRegion(center: CLLocationCoordinate2DMake(fav.latitude, fav.longitude), radius: 100, identifier: str)
                    self.manager.startMonitoring(for: geoFencing)
                }
            } else {
                // Log details of the failure
                print("Oops \(error!)")
            } }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
