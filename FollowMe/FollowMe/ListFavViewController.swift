//
//  ListFavViewController.swift
//  FollowMe
//
//  Created by Student on 4/2/18.
//  Copyright © 2018 temM5. All rights reserved.
//

import UIKit
import Parse
class ListFavViewController: UIViewController, UITableViewDelegate,   UITableViewDataSource {
    
    var favs : [Favourite] = [];
    
    @IBOutlet weak var favTable: UITableView!
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return favs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "favTable", for: indexPath)
        cell.textLabel?.text = favs[indexPath.row].title
        cell.detailTextLabel?.text = favs[indexPath.row].city
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let alert = UIAlertController(title: "\(favs[indexPath.row].title)", message: "\(favs[indexPath.row].address)", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        
        self.present(alert, animated: true)
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.fetch();
        // Do any addiational setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func fetch() {
        let query = PFQuery(className:"Favourite")     // Fetches all the Movie objects
        let user = AppDelegate.model?.currUser
        query.whereKey("userId", equalTo: user!.objectId ?? "" )
        query.findObjectsInBackground {   // what happened to the ( ) ?
            (objects: [PFObject]?, error: Error?) -> Void in
            if error == nil {
                // The find succeeded.
                self.favs = objects as! [Favourite]
                // Do something with the found objects
                // Like display them in a table view.
                self.favTable.reloadData()
            } else {
                // Log details of the failure
                print("Oops \(error!)")
            } }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
