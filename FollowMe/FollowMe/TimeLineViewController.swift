//
//  TimeLineViewController.swift
//  FollowMe
//
//  Created by student on 4/22/18.
//  Copyright © 2018 temM5. All rights reserved.
//

import UIKit
import MapKit
import Parse

class TimeLineViewController: UIViewController {
    var favs : [Favourite] = []
    @IBOutlet weak var timeLineMP: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
         self.fetchLoc()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func fetchLoc(){
        let query = PFQuery(className:"Favourite")     // Fetches all the Favourites objects
        query.findObjectsInBackground {   // what happened to the ( ) ?
            (objects: [PFObject]?, error: Error?) -> Void in
            if error == nil {
                // The find succeeded.
                self.favs = objects as! [Favourite]
                for fav in self.favs{
                    print(fav)
                    var pin:AnnotationPin!
                    let coordinate = CLLocationCoordinate2D(latitude: fav.latitude, longitude:  fav.longitude )
                    pin = AnnotationPin(title: fav.street, Subtitle: "", coordinate: coordinate)
                    self.timeLineMP.addAnnotation(pin)
                }
            } else {
                // Log details of the failure
                print("Oops \(error!)")
            } }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
