//
//  AddFavLocationViewController.swift
//  FollowMe
//
//  Created by student on 3/28/18.
//  Copyright © 2018 temM5. All rights reserved.
//

import UIKit
import MapKit

var fav = Favourite()
var alertFlag = false // for validation

class AddFavLocationViewController: UIViewController {
    
    @IBOutlet weak var selectedAddLBL: UILabel!
    @IBOutlet weak var printAddLBL: UITextView!
    @IBOutlet weak var favNameLBL: UILabel!
    @IBOutlet weak var favNameTF: UITextField!
    
    @IBOutlet weak var searchResultsTableView: UITableView!
    var searchCompleter = MKLocalSearchCompleter()
    var searchResults = [MKLocalSearchCompletion]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        var user = AppDelegate.model?.currUser
        searchCompleter.delegate = self
        self.title = "Add Favourites"
        
//    hiding labels and text field before selecting
        selectedAddLBL.isHidden = true
        printAddLBL.isHidden = true
        favNameLBL.isHidden = true
        favNameTF.isHidden = true
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        fav = Favourite()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func storeFavouries(sender: AnyObject) {
        
        if alertFlag {
            let user = AppDelegate.model?.currUser
            fav["userId"] = user!.objectId
            if favNameTF.hasText {
                fav["title"] = self.favNameTF.text
                print(fav.title)
                fav.saveInBackground(block: { (success, error) -> Void in
                    if success {
                        print("Favourites saved.")
                        fav = Favourite()
                    } else {
                        print("error!!!")
                    }
                })
            } else {
                let alert = UIAlertController(title: "Opps!", message: "Enter the Favourite Name", preferredStyle: .alert)
            
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            
                self.present(alert, animated: true)
            }
        } else {
            let alert = UIAlertController(title: "Opps!", message: "Select Favorite Location\nNo location was slected", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            
            self.present(alert, animated: true)
        }
    }

}


extension AddFavLocationViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print(searchText)
        searchCompleter.queryFragment = searchText
    }
}

extension AddFavLocationViewController: MKLocalSearchCompleterDelegate {
    
    func completerDidUpdateResults(_ completer: MKLocalSearchCompleter) {
        searchResults = completer.results
        searchResultsTableView.reloadData()
    }
    
    func completer(_ completer: MKLocalSearchCompleter, didFailWithError error: Error) {
        // handle error
    }
}

extension AddFavLocationViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchResults.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let searchResult = searchResults[indexPath.row]
        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: nil)
        cell.textLabel?.text = searchResult.title
        cell.detailTextLabel?.text = searchResult.subtitle
        return cell
    }
}

extension AddFavLocationViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
//        validation by setting alertflag true
        alertFlag = true
        
        let completion = searchResults[indexPath.row]
        
        let searchRequest = MKLocalSearchRequest(completion: completion)
        let search = MKLocalSearch(request: searchRequest)
        search.start { (response, error) in
//            adding the loaction details from search
            fav["latitude"] = response?.mapItems[0].placemark.coordinate.latitude
            fav["longitude"] = response?.mapItems[0].placemark.coordinate.longitude
            
//            spliting the adress
            let address = response?.mapItems[0].placemark.title
            fav["address"] = address
            let titleSplit = address?.split(separator: ",")
            fav["street"] = titleSplit![0]
            fav["city"] = titleSplit![1]
            fav["state"] = titleSplit![2]
            fav["country"] = titleSplit![3]
            
//            print(String(describing: title?.split(separator: ",")))
            print(fav.street)
            print(fav.city)
            print(fav.state)
            print(fav.country)
            
//            hide the tabelview
            self.searchResultsTableView.isHidden = true
            self.viewDidLoad()
            
//            selected address
//            showing labels and text field before selecting
            self.selectedAddLBL.isHidden = false
            self.printAddLBL.isHidden = false
            self.favNameLBL.isHidden = false
            self.favNameTF.isHidden = false
            self.printAddLBL.text = address
        }
    }
}


