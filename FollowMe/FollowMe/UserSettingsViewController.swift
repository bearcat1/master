//
//  UserSettingsViewController.swift
//  FollowMe
//
//  Created by Agrawal,Abhijeet P on 4/22/18.
//  Copyright © 2018 temM5. All rights reserved.
//

import UIKit
import GoogleSignIn
class UserSettingsViewController: UIViewController,GIDSignInUIDelegate {
    
    var window : UIWindow?
    override func viewDidLoad() {
        super.viewDidLoad()
        var user = AppDelegate.model?.currUser
        userName.text = user!.firstName + " " + user!.lastName
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBOutlet weak var userName: UILabel!
    
    
    @IBAction func logOut(_ sender: AnyObject){
        GIDSignIn.sharedInstance().signOut()
        let mainStoryboardIpad : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let initialViewControlleripad : UIViewController = mainStoryboardIpad.instantiateViewController(withIdentifier: "login") as UIViewController
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window?.rootViewController = initialViewControlleripad
        self.window?.makeKeyAndVisible()
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
