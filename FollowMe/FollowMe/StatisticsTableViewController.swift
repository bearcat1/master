//
//  StatisticsTableViewController.swift
//  FollowMe
//
//  Created by Student on 4/2/18.
//  Copyright © 2018 temM5. All rights reserved.
//

import UIKit
import Parse

class StatisticsTableViewController: UIViewController , UITableViewDelegate,   UITableViewDataSource{
    var favs : [PlacesVisited] = [];
    
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var statsTable: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        let calendar = Calendar.current
        
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        datePicker.addTarget(self, action: #selector(dateChanged(_:)), for: .valueChanged)
        
       
//        if let day = components.day, let month = components.month, let year = components.year {
//            print("\(day) \(month) \(year)")
//            let key = "\(year)-\(month)-\(day)"
//            self.fetch(key)
//        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let timer = Date()
        let dateStr:String = String(describing: timer)
        var dateA  = dateStr.split(separator : " ")
        self.fetch(String(dateA[0]))
    }
    @objc func dateChanged(_ sender : UIDatePicker){
        let componenets = Calendar.current.dateComponents([.year, .month, .day], from: sender.date)
        if let day = componenets.day, let month = componenets.month, let year = componenets.year {
            print("\(day) \(month) \(year)")
            var key = "\(year)-\(month < 10 ? "0" : "")\(month)-\(day < 10 ? "0" : "")\(day)"
            self.fetch(key)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

  
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return favs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "statsTable", for: indexPath)
        cell.textLabel?.text = "\(favs[indexPath.row].placeTitle)"
        cell.detailTextLabel?.text = String(format: "%.2f", favs[indexPath.row].timeSpent/60.0)
        return cell
    }
    
    func fetch(_ key:String) {
        print("Called \(key)")
        let user = AppDelegate.model?.currUser
        let query = PFQuery(className:"PlacesVisited")     // Fetches all the Favourites objects
        query.whereKey("userId", equalTo: user!.objectId ?? "" )
        query.whereKey("date", equalTo: key )
        query.findObjectsInBackground {   // what happened to the ( ) ?
            (objects: [PFObject]?, error: Error?) -> Void in
            if error == nil {
                // The find succeeded.
                self.favs = objects as! [PlacesVisited]
                
                // Do something with the found objects
                // Like display them in a table view.
                self.statsTable.reloadData()
            } else {
                // Log details of the failure
                print("Oops \(error!)")
            } }
    }

}
